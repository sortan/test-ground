import os
import json

import pylab
import numpy as np
import wave

from pydub import AudioSegment


'''
TODO: Convert this in to a helper class.
- NFFT should be 1024?
- window use hamming-1024
'''


def load_wav(file_path: str):
	# check path.
	if not os.path.exists(file_path):
		raise FileExistsError('The file path does not exist!')

	# load as AudioSegment
	return AudioSegment.from_wav(file_path)

def stereo_to_mono(audio: AudioSegment):
	if audio.channels == 1:
		return audio
	elif audio.channels == 2:
		return audio.set_channels(1)
	else:
		raise ValueError('audio channel can only be 1 or 2!')

def low_pass_filter(audio: AudioSegment, cut_off: int) -> AudioSegment:
	return audio.low_pass_filter(cut_off)

def downsample(audio: AudioSegment, frame_rate: int):
	return audio.set_frame_rate(frame_rate)

def get_wav_info(audio: AudioSegment):
	# convert the pydub samples array to numpy array for later uses.
	return np.array(audio.get_array_of_samples()), audio.frame_rate

def gen_spectrogram(samples, sample_rate: int ):
	pylab.figure(num=None, figsize=(19, 12))
	pylab.subplot(111)
	pylab.title('spectrogram')
	spectrum, frequency, time, _ = pylab.specgram(
		samples,
		Fs=sample_rate,
		# as defined in the paper we going to use 1024 bin.
		window= np.hamming(1024),
		NFFT= 1024,
		# in the recent article, it mention to use the spectrum magnitude.
		# mode='magnitude',
		# need to add overlap 0 too to get what the paper wanted.
		noverlap = 0
	)
	# save to json.
	# data = {
	# 	'spectrum': spectrum.tolist(),
	# 	'frequency': frequency.tolist(),
	# 	'time': time.tolist()
	# }
	# data_json = json.dumps(data)
	# with open('share/json/test_spec.json', 'w') as f:
	# 	f.write(data_json)
	pylab.savefig('spectrogram.png')

# def graph_spectrogram(wav_file):
# 	sound_info, frame_rate = get_wav_info(wav_file)
# 	pylab.figure(num=None, figsize=(19, 12))
# 	pylab.subplot(111)
# 	pylab.title('spectrogram of %r' % wav_file)
# 	pylab.specgram(sound_info, Fs=frame_rate)
# 	pylab.savefig('spectrogram.png')

# def load_wave(wav_file):
# 	wav = wave.open(wav_file, 'r')
# 	frames = wav.readframes(-1)
# 	# this will get frames but not in a binary form.
# 	sound_info = pylab.fromstring(frames, 'int16')
# 	frame_rate = wav.getframerate()
# 	wav.close()
# 	return sound_info, frame_rate
