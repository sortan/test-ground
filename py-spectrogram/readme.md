This section still need a lot of digging.

Currently, we are following a bunch of method to generate spectogram. And they are (the first of the list is one that are likely to be implemented):
- [How to convert a .wav file to a spectrogram in python3](https://stackoverflow.com/questions/44787437/how-to-convert-a-wav-file-to-a-spectrogram-in-python3). This did not work as well as we hope for. The problems are that it only work on mono audio file, but we assume that all of the method can be only be read using mono type. However, the main problem is that it did not produce a spectrogram. Let try with a `window function` and other stuff by following the [documentation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html).
- [Python - How to save spectrogram output in a text file?](https://stackoverflow.com/questions/47846320/python-how-to-save-spectrogram-output-in-a-text-file). This will be a way to store spectrogram data.
- [generating spectrograms in python with less noise](https://dsp.stackexchange.com/questions/10743/generating-spectrograms-in-python-with-less-noise)
- Will try methods from the `matplotlib` documentation.

Other sources to look at:
- We need to perform a low-pass filter so that our downsampling won't produce any unwanted noise. As the `How Shazam work?` suggested, we need to perform a low-pass filter to truncate any frequency below 5kHz.
- As the author of the `How Shazam work?` had mention, the spectrogram will be generated on mono audio, and it work on **44kHz** frame-rate. So to convert this since we use the [python wave module](https://docs.python.org/3/library/wave.html), we will try many way of frame rate convertion and see which one work best starting from [here](https://stackoverflow.com/questions/30619740/downsampling-wav-audio-file).


We are trying to produce the steps that was taking by this:
![steps](./assets/steps.png)
We will put everything in a helpers module just to make think simplify


We are also Following a books call [**ThinkDSP**](https://greenteapress.com/thinkdsp/thinkdsp.pdf). With the example code and excercises [*here*](https://github.com/AllenDowney/ThinkDSP).
