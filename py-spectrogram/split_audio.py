from pydub import AudioSegment
t1 = 2 * 1000 #Works in milliseconds
t2 = 8 * 1000
newAudio = AudioSegment.from_wav("share/wav/[MV] IU(아이유) _ Celebrity_1theK (원더케이).wav")
newAudio = newAudio[t1:t2]
newAudio.export('segment_clip.wav', format="wav") #Exports to a wav file in the current path.
