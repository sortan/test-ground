import os
import wave

import helpers


wave = helpers.load_wav('share/wav/stereo/[MV] IU(아이유) _ Celebrity_1theK (원더케이).wav')
mono = helpers.stereo_to_mono(wave)
mono = helpers.low_pass_filter(mono, 5000)
mono = helpers.downsample(mono, 11025)
samples, sample_rate = helpers.get_wav_info(mono)
helpers.gen_spectrogram(samples, sample_rate)

# mono.export("share/wav/mono/[MV] IU(아이유) _ Celebrity_1theK (원더케이)_mono.wav", format="wav")
