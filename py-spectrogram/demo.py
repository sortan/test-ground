import os
import wave
from pydub import AudioSegment

wav = AudioSegment.from_wav('share/wav/mono/[MV] IU(아이유) _ Celebrity_1theK (원더케이).wav_mono.wav', 'r')
new_wav = wav.low_pass_filter(5000)
new_wav.export('share/wav/mono/[MV] IU(아이유) _ Celebrity_1theK (원더케이)_filter.wav', 'wav')
