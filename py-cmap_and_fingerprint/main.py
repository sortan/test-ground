import statistics
import json

from pydub import AudioSegment
import pylab
import numpy as np
import matplotlib.pyplot as plt

from helpers import (
  HighestAmplitudePeak,
  get_max_amplitude_between_freq,
  FingerPrint
)

'''
TODO: keep and eyes on the spectrogram mode, now we are using psd it could
be magnitude tho since other people suggested it.
'''

# load the filter wav file
audio = AudioSegment.from_wav('share/wav/mono/[MV] IU(아이유) _ Celebrity_1theK (원더케이)_mono.wav')

# get frames and frame_rate.
frames, frame_rate = np.array(audio.get_array_of_samples()), audio.frame_rate

# get spectrum, frequency, time from spectrogram. 
# spectrum have column as frequency and row as time.
spectrum, frequencies, times, _ = pylab.specgram(
  frames,
  Fs=frame_rate,
  # as defined in the paper we going to use 1024 bin.
  window= np.hamming(1024),
  NFFT= 1024,
  # in the recent article, it mention to use the spectrum magnitude.
  # mode='magnitude',
  # need to add overlap 0 too to get what the paper wanted.
  noverlap = 0,
)

# save to json.
# data = {
#   'spectrum': spectrum.tolist(),
#   'frequency': frequencies.tolist(),
#   'time': times.tolist()
# }
# data_json = json.dumps(data)
# with open('share/json/test_spec_downsample.json', 'w') as f:
#   f.write(data_json)

'''
for some reason the frequency length is 513, it should be 512 because it is
Number-of-Bin = Window-size / 2 in this case it is 1024 / 2.
so here are the step to generate the contellation map:
  + for each time unit (0.1s) we group the frequency into 6 band:
    - the very low sound band (from freq bin 0 to 10)
    - the low sound band (from freq bin 10 to 20)
    - the low-mid sound band (from freq bin 20 to 40)
    - the mid sound band (from freq bin 40 to 80)
    - the mid-high sound band (from freq bin 80 to 160)
    - the high sound band (from freq bin 160 to 511)
  + keep the strongest amplitude from each band.
  + calculate the mean of the strongest amplitude.
  + 1 if strongest-amplitude > mean else 0
'''
# create new numpy array with the same dimension of spectrum. for PLOTTING purpose only.
highest_peaks = []

# get each time frame.
'''
TODO: DEBUG this shit. (DONE)
- write a helper function that get the max value from each band.
If the the band is empty return 0.
- when the qualify_band_idexes max value is 0, then no (time, frequency) pair need to be added.
- when qualify_band_idexes is empty, then no (time, frequency) pair need to be added.

'''
for index in range(times.size - 1):
  time_frame = spectrum[:, index]
  time = times[index]

  # get the the 6 bands of time frame.
  vl_band = get_max_amplitude_between_freq(time_frame, 0, 10)
  l_band = get_max_amplitude_between_freq(time_frame, 10, 20)
  lm_band = get_max_amplitude_between_freq(time_frame, 20, 40)
  m_band = get_max_amplitude_between_freq(time_frame, 40, 80)
  mh_band = get_max_amplitude_between_freq(time_frame, 80, 160)
  # why the hell it is 513 =_=
  h_band = get_max_amplitude_between_freq(time_frame, 160, 513)
  # calculate the amplitude mean of the time frame.
  bands = np.array([
    vl_band,
    l_band,
    lm_band,
    m_band,
    mh_band,
    h_band
  ])
  if bands.max() > 0:
    mean_amplitude = statistics.mean(bands)
    '''
    find the index of band that the amplitude is higher then the mean.
    in the paper, the mean is multiply by a coefficient.
    it is also mention that this particular algorithm is not very efficient.
    it can be change to `logarithmic sliding window and to keep only the most powerful
    frequencies above the mean + the standard deviation (multiplied by a coefficient) of
    a moving part of the song.`
    '''
    qualify_band_idexes = np.where(bands > mean_amplitude)[0]
    # get the qualify amplitude in the time frame.
    qualify_amplitudes = bands[qualify_band_idexes]
    # get the frequency indexes in the time frame that will product the qualify amplitude.
    high_peaks_freq_idexes = np.nonzero(np.in1d(time_frame, qualify_amplitudes))[0]
    '''
    Before the grouping into target zone, we need to sort the our time-frequency pair first.
    - For each time-frame we it is sorted by low freq to high freq.
    - For each spectrogram, we sorted it by lowest-time to highest-time.
    '''
    # get frequenies by index in the time frame and sort it.
    high_peaks_freq = np.sort(frequencies[high_peaks_freq_idexes])
    # loop through those index and create a coordinate for it (time, frequency)
    for freq in high_peaks_freq:
      highest_peaks.append(HighestAmplitudePeak(
        time,
        freq
      ))
# form the x axis for ploting in our case x axis is time.
x_axis = [peak.time for peak in highest_peaks]
# form the y axis for ploting in our case y axis is frequency.
y_axis = [peak.frequency for peak in highest_peaks]

'''Plotting the filtered spectrogram.'''
plt.scatter(x_axis, y_axis, s=0.5, c='red')
plt.show()

'''Grouping target zone'''
target_zones = []
ZONE_LENGTH = 5
ANCHOR_LEN_DIFF = 3
# loop through highest_peaks and create target-zone
# we loop through the index instead because we want to look ahead and add them to the same
# target-zone. And this case we remove `4` from the length so that the 4 index won't form
# any out of range target-zone.
for peaks_index in range(len(highest_peaks) - (ZONE_LENGTH - 1)):
  # get the anchor point for this particular zone.
  anchor_point = highest_peaks[peaks_index - ANCHOR_LEN_DIFF] if peaks_index > ANCHOR_LEN_DIFF else highest_peaks[0]
  # create the fingerprint for each point in the zone.
  for i in range(ZONE_LENGTH):
    target_zones.append(FingerPrint(
      highest_peaks[peaks_index + i],
      anchor_point,
      1 # could be any song id in the server.
    ))

print(highest_peaks[-1])
print(target_zones[-1])
