we got data:
1, 2, 3, 4, 5, 6, 7, 8, 9, 10. total length: 10

group by 2 we got:
1-2, 2-3, 3-4, 4-5, 5-6, 6-7, 7-8, 8-9, 9-10. total length: 9

group by 3:
1-2-3, 2-3-4, 3-4-5, 4-5-6, 5-6-7, 6-7-8, 7-8-9, 8-9-10. total length: 8

So the length change by the highest-point (3) - lowest-point (1) = 2
then 10 - 2 = 8

### process of assigning anchor peak.
anchor_index should be 3 before but in the rare case of the starting point:
if index = 0, anchor_index = 0
if index = 1, anchor_index = 0
if index = 2, anchor_index = 0
if index = 3, anchor_index = 0
if index = 4, anchor_index = 1
if index = 5, anchor_index = 2
