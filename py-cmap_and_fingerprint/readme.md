### CMAP & FINGERPRINT

This section will deal with:
- filtering the spectrogram to a constilation map by filtering the highest amplitude at each 0.1 second.
- grouping the highest peaks to target zones.
- generate fingerprint from target zones.


#### constilation map (under REVIEW)
We achieve this by:
![constilation map step](./assets/cmap_step.png)

#### Target zone and Figerprint:
- Target-zone can be created by grouping 5 time-freq pair with the initial point going forward 4 more time.
- Each target-zone have an anchor point. It can be any point outside the target zone. This could be an exception for the the first point in the clip, because there is no point before and the point in the clip because there is no other point after it.
- Each point in the target-zone have an address that is pair with the anchor point: \
  ***[anchor_pint_freq, point_in_target_zone_freq, time_differences]***.
- And with each ***addresses*** we can pair it with *couple* which are \
  ***[abs_anchor_point_time, song_id]***. In the SQL database, we can just group th *address* and *couple* into 1 single data type with all of the attribute needed.
- We can also store the total peaks per song in the Songs table and the average peaks per time interval of each song and then we can calculate the mean of the average peak per song, we might be able to use it in later time.
- And we can specify our data type for freq and time to minimize the storage consumption or just store in the data type that Numpy have gave us. We can also make an assumtion of the maximum time that a song can be slient so we can gave the most optimize data type to the **delta-time-difference** but would exclude some of the song.

#### searching and scoring the possible song:
- From the mobile device, we can generate the **addresses** and the **the-abs-anchor-time** (maybe the number of target-zone in the clip too) and send to the server.
- With the **addresses** we can perform a database look up and get the **couples** pair.
- With the **couples** that was found in the database, we can iterate through them and eliminate any **couple** that appear less then *4-5 times* because as we had established is that a **peak** can be appear **atmost 5 time** since it group with 4 other peaks ahead of it to form a target-zone. The peaks that appear lest then 5 time are the first 4 peaks and the last 4 peaks. Why did we eliminate it? Because if a **couple** appear less than that it can most likely won't be able to form a target zone. (sidenote: If the target-zone is appear more (it should not because no 2 target-zone have the same anchor point) maybe we can spit the it to more target-zone?? )
- Now that we group the **couples** in **target-zones** we can count how many target zone did we grouped. And it it less than the **amount-of-target-zone * coefficion**, we will eliminate those song too. (coefficion here is to reduce the number of target-zone because of the noise. we can also add an error interval instead too.)
- now that we got the **song_id** that are qualify, the only issue left to align all the **song_id's target-zones** together and figure out which one is the correct one.
- It is possible that an **address** in the recorded audio match many of the **couples** in the song that was return to us.
- So, what we are going to do is calculate each **abs_anchor_time_record** of the record to every **abs_anchor_time_music** that we get. The calculation is just to find the starting time `delta` = `abs_anchor_time_record` - `abs_anchor_time_music` and store all `delta` in `delta_list`.
- After we loop through the `records peaks` we find the `delta-music_id` pair that appear the most, from that it just a matter of `music_id` lookup. \
  **sidenote:** the final step can be use in Numpy to speedup the calculation. Now it is a matter of arraging the matrices.


**SIDENOTE:** For the searching part of the program, the SQL should be:

SELECT fp FROM Fingerprints WHERE \
(fp.ach_freq == given_ach_freq1 && \
fp.point_freq == given_freq1 && \
fp.time_diff == given_time_diff1) ||
(fp.ach_freq == given_ach_freq2 && \
fp.point_freq == given_freq2 && \
fp.time_diff == given_time_diff2) || ... \
Groupby fp.abs_ach_time

#### Problem that can be encounter that it did not mention in the paper:
- **anchor point duplication:** The anchor point is associated to 1 target zone and it should be `unique` in order to make the grouping stage of the `search` easier or we would have to devide the larger groups into small differnence groups. This uniqueness seem to be depending the `anchor_point_time` as shown in the grouping explaination. I assume this is why when filtering for the highest peaks, The author only devided in to 6 bands of frequency, so each `time_frame` at worst it have 5 peaks per time_frame (due to how the mean work). However the author also suggest that we can define how many `bands` we going to use, but did not clarify if it going to effect the `target_zone` size or not (Or maybe I did not read that part... who know...). And from inspection, it should affect cause of the uniqueness of the anchor_point. Furthermore, the author also suggest the filtering methods where we only kept the `freqencies` that 1 standard deviation higher within the time frame. We did not experiment on how it would affect the target_zone grouping or how it would affect our searching stage.
