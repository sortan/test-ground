## This is a repo for testing purposes only!
#### Notice:
The python version is 3.7, which is the version with most support right now.

**rules:**
- have a functionality to test please create a separate directiory to do so.
- Each test directory should have a `readme` for a short description, summary, code example snippet, or any importance details about resouces for download.
- `share` directory is for sharing any cross test. However add labels for new directory.
- git ignore any data, please add a link for download instead in the test directory `readme`.
- when testing a functionality please do so on it own branch. The branch name should look like `directory-name_functionality` then create a pull request where other can review and merge into `master` afterward.

**todo:**
1. Set up another project management for development only.
2. After the task `1` is finish, tasks `3` onward should be migrated to it
and the *`todo`* can be remove.
3. work with downloading the youtube audio with youtube-dl.
4. work on generating spectrogram.
5. look for a way to download audio from playlist where the video is unavailable

