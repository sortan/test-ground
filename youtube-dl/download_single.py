from youtube_dl import YoutubeDL
import json
import os

CUR_PATH = os.getcwd() + '/share/mp3'

ydl_opts = {
  'noplaylist': True,
  'extract-audio': True,
  'audio-format': 'mp3',
  # 'format': 'm4a',
  'postprocessors': [{
    'key': 'FFmpegExtractAudio',
    'preferredcodec': 'mp3',
    'preferredquality': '192',
  }],
  'outtmpl': CUR_PATH + '/%(title)s_%(uploader)s.%(ext)s',
  # 'outtmpl': CUR_PATH + '/%(title)s_%(uploader)s.mp3',
}

video = 'https://www.youtube.com/watch?v=0-q1KafFCLU'
with YoutubeDL(ydl_opts) as ydl:
  info_dict = ydl.extract_info(video, download=True)
  # print('info_dict: ', info_dict)
  video_url = info_dict.get('url', None)
  print('video_url: ', video_url)
  video_id = info_dict.get('id', None)
  print('video_id: ', video_id)
  video_title = info_dict.get('title', None)
  print('video_title: ', video_title)
  video_uploader = info_dict.get('uploader')
  video_length = info_dict.get('duration')
  print('video_length', video_length)

  # with open(f'share/mp3_info_dict/{video_title}_{video_uploader}.json', 'w') as wFile:
  #   wFile.write(json.dumps(info_dict))

# print(video_title)
